package com.dhc.chapter02;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;

/**
 * 多线程Executor框架的使用
 * 
 * @author wangshuaishuai
 *
 */
public class ExecutorDemo {

	private static Integer pages=1;
	private static boolean  exeFlag=true;
	
	public static void main(String[] args) {

		ExecutorService executorService = Executors.newFixedThreadPool(10);
	
		while(exeFlag){
			if(pages<=100){
				executorService.execute(new  Runnable() {
					public void run() {
						System.out.println("正在爬取第"+pages+"个网页....");
						pages++;
						
					}
				});
			}else{
				ThreadPoolExecutor poolExecutor=(ThreadPoolExecutor)executorService;
				if(poolExecutor.getActiveCount()==0){
					exeFlag=false;
					executorService.shutdown();
					System.out.println("爬虫任务已经完毕！");
				}
			}

			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
	}

}
