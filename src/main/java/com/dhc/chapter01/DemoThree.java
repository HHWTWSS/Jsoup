package com.dhc.chapter01;

import org.apache.http.HttpEntity;
import org.apache.http.util.EntityUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.dhc.util.HttpClientUtil;

/**
 * 在Jsoup中使用选择器
 * 
 * @author wangshuaishuai
 *
 */
public class DemoThree {

	public static void main(String[] args) throws Exception  {
		HttpEntity html = HttpClientUtil.getContextByUrl("https://www.cnblogs.com/");
		Document document = Jsoup.parse(EntityUtils.toString(html, "UTF-8"));
		Elements elements = document.select("#post_list .post_item_body h3 a");
		for (Element element : elements) {
			System.out.println(element.text());
		}
		System.out.println("---------------------------------------------------------------------------------");
		// 获取带有herf属性的元素
		Elements elments = document.select("a[href]");
		for (Element element : elements) {
			System.out.println(element.toString());
		}
		System.out.println("---------------------------------------------------------------------------------");
		// 获取扩展名为.png的图片
		elements = document.select("img[src$=.png]");
		for (Element element : elements) {
			System.out.println(element.toString());
		}
		System.out.println("---------------------------------------------------------------------------------");
		//获取第一个元素的方法
		 elments=document.getElementsByTag("title");
		 Element element=elments.first();
		 System.out.println("网页标题为"+element.text());
	}
}
