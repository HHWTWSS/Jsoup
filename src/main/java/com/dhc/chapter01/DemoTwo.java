package com.dhc.chapter01;

import org.apache.http.HttpEntity;
import org.apache.http.util.EntityUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.dhc.util.HttpClientUtil;

/**
 * document.getElementById(id)
 * document.getElementsByTag(tagName)
 * document.getElementsByClass(className));
 * document.getElementsByAttribute(key)
 * document.getElementsByAttributeValue(key, value)
 * api的使用
 * @author wangshuaishuai
 *
 */
public class DemoTwo {

	public static void main(String[] args) throws Exception{
		// TODO Auto-generated method stub
		
	      HttpEntity html=HttpClientUtil.getContextByUrl("https://www.cnblogs.com/");
	      Document document=Jsoup.parse(EntityUtils.toString(html,"UTF-8"));
	     Element element1= document.getElementById("site_nav_top");
	     System.out.println("根据ID获取元素的文本"+element1.text());
	     Elements elements= document.getElementsByTag("body");
	     Element element2=elements.get(0);
	     System.out.println("根据标签获取元素的HTML"+element2.html());
	     Elements elements2=document.getElementsByClass("post_nav_block");
	     Element element3=elements2.get(0);
	     System.out.println("根据class属性获取元素的HTML"+element3.html());
	     Elements elements3=document.getElementsByAttribute("id");
	     Element element4=elements3.get(0);
	     System.out.println("根据属性获取元素的HTML"+element4.html());
	     Elements elements5=document.getElementsByAttributeValue("class", "post_item_body");
	     Element element5=elements5.get(0);
	     System.out.println("根据属性名和属性值获取元素的HTML"+element5.html());
	      
	    
	}

}
