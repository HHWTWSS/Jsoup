package com.dhc.chapter01;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.apache.commons.io.FileUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.ParseException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.dhc.util.HttpClientUtil;

/**
 * 获取91 网站的前20页所有色情图片
 * 
 * @author wangshuaishuai
 *
 */
public class ExerciseTwo {

	public static void main(String[] args) throws Exception {
		long start = System.currentTimeMillis();
		System.out.println("下载美女图片开始.....");
		int sum=1;
		for (int i = 1; i <= 20; i++) {
			String url = "http://92.91p31.space/v.php?next=watch&page="+i+"";
			HttpEntity content = HttpClientUtil.getContextByUrl(url);
			String html=EntityUtils.toString(content, "UTF-8");
			Document document = Jsoup.parse(html);
			String fileName = null;
			Elements imgElements = document.select("img[src$=.jpg]");
			for (int j = 1; j < imgElements.size(); j++) {
				String src = imgElements.get(j).attr("src");
				
				content = HttpClientUtil.getContextByUrl(src);
				fileName = imgElements.get(j).attr("title");
				FileUtils.copyInputStreamToFile(content.getContent(), new File("d://91//" + UUID.randomUUID().toString()+".jpg"));
				System.out.println("恭喜你!第" + sum + "張"+UUID.randomUUID().toString()+"女图片下载成功！");
				sum++;
			}

		}
		long end = System.currentTimeMillis();
		System.out.println("下载美女图片共耗时" + (end - start) / 1000 + "秒");
	}

	

}
