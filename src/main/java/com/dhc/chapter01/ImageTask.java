package com.dhc.chapter01;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;

public class ImageTask implements Runnable {

	private HttpGet httpGet=null;
	private HttpResponse response=null;
	private HttpEntity	entity=null;
	private String url=null;
	
	private  static CloseableHttpClient httpClient=HttpClients.createDefault();;
	
	public ImageTask(String url) {
		super();
	    this.url = url;
	}

	@Override
	public void run() {
		
		this.httpGet=new HttpGet(this.url);
		
		try {
			this.response = httpClient.execute(httpGet);
			this.entity=this.response.getEntity();
			String fileName=this.url.substring(this.url.lastIndexOf("/")+1);
			FileUtils.copyInputStreamToFile(entity.getContent(),new File("d://beauties//"+fileName));
		
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
