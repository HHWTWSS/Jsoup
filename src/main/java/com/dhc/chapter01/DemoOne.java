package com.dhc.chapter01;

import java.io.IOException;

import org.apache.http.HttpEntity;
import org.apache.http.ParseException;
import org.apache.http.util.EntityUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.dhc.util.HttpClientUtil;

/**
 * 使用Jsoup获取网页标题
 * @author wangshuaishuai
 *
 */
public class DemoOne {

	public static void main(String[] args) throws Exception, IOException {
		
      HttpEntity html=HttpClientUtil.getContextByUrl("http://ylxt.dhcc.com.cn/ylxt/");
      Document document=Jsoup.parse(EntityUtils.toString(html, "UTF-8"));
      Elements elments=document.getElementsByTag("title");
      Element elment=elments.get(0);
      String text=elment.text();
      System.out.println("标题为:"+text);
      
	}

}
