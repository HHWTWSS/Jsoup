package com.dhc.chapter01;

import java.io.IOException;

import org.apache.http.HttpEntity;
import org.apache.http.ParseException;
import org.apache.http.util.EntityUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.dhc.util.HttpClientUtil;

/**
 * 使用Jsoup获取Dom元素的属性
 * 
 * @author wangshuaishuai
 *
 */
public class DemoFour {

	public static void main(String[] args) throws Exception, IOException {
		HttpEntity content = HttpClientUtil.getContextByUrl("https://www.cnblogs.com/");
		Document document = Jsoup.parse(EntityUtils.toString(content,"UTF-8"));
				
		Elements elements = document.select("#friend_link a");
		for (Element element : elements) {
			String href=element.attr("href");
			String text=element.text();
			String target=element.attr("target");
			System.out.println("herf="+href+"   text="+text+"target="+target);
		}
	}

}
