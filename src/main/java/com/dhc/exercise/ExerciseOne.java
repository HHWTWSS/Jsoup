package com.dhc.exercise;

import java.io.File;
import java.io.InputStream;
import java.util.UUID;

import org.apache.commons.io.FileUtils;
import org.apache.http.HttpEntity;
import org.apache.http.util.EntityUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Attributes;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.dhc.util.HttpClientUtil;

/**
 * 练习：爬取91 网站首页所有可以查看视频的超链接
 * @author wangshuaishuai
 *
 */
public class ExerciseOne {

	public static void main(String[] args) throws Exception {
		HttpEntity context = HttpClientUtil.getContextByUrl("http://92.91p31.space/v.php?next=watch&page=1");
		Document document = Jsoup.parse(EntityUtils.toString(context,"UTF-8"));
	
		Elements elements = document.select(".listchannel a");
		String file_name=null;
		Element element=null;
		
		int count=0;
		for (int i = 0, size = elements.size(); i < size; i++) {
			element=elements.get(i);
			String herf=element.attr("href");
			context = HttpClientUtil.getContextByUrl(herf);
			System.out.println(document);
			element = document.getElementsByTag("source").first();
			String source=element.attr("src");
			context = HttpClientUtil.getContextByUrl(source);
			InputStream  stream=context.getContent();
			FileUtils.copyInputStreamToFile(stream, new File("D:/videos/"+UUID.randomUUID().toString()+".mp4"));
			System.out.println("恭喜你 !第"+count+"文件"+file_name+"下载成功！");
		}
	}
}
