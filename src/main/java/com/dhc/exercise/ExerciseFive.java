package com.dhc.exercise;

import java.util.HashMap;
import java.util.Map;

import org.apache.http.HttpEntity;
import org.apache.http.util.EntityUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import com.dhc.util.HttpClientUtil;

/**
 * 根据 手机号码获取归属地等相关信息
 * 
 * @author wangshuaishuai
 *
 */
public class ExerciseFive {

	public static void main(String[] args) throws Exception {

		Map<String, String> params = new HashMap<String, String>();
		params.put("mobile", "15156852862");
		params.put("action", "mobile");
		HttpEntity entity = HttpClientUtil.getContextByUrl("http://www.ip138.com:8080/search.asp", params);
		String response=EntityUtils.toString(entity,"gb2312");
		Document document=Jsoup.parse(response);
		Elements elements=document.select("table tbody tr");
		for (int i = 0; i < elements.size(); i++) {
			System.out.println(elements.get(i).text());
		}
	}

}
